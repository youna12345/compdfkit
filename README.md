# 1 Overview

PDF Technologies has focused on PDF for years. The PDF solution ComPDFKit includes PDF SDK, Conversion SDK, Web Viewer, API Tools, Free Online Converter, and Document AI. We can also provide Software, SDK, and API solutions for industries like education, government, Construction, Aviation, etc.

ComPDFKit is the SDK and API product we provided that allows developers or companies to integrate PDF editing, annotating, converting, form filling, digital signing, comparing, measuring, and redacting into any device.

**ComPDFKit Products Include:**

- ComPDFKit SDK: ComPDFKit allows companies, organizations, small businesses, and developers to integrate viewing, annotating, editing, converting (PDF to/from Office, Images, HTML, RTF, CSV, PDF/A, etc.), filling, encrypting, merging, comparing, redacting, OCR, and signing PDF documents in their applications or products.

- ComPDFKit API: Make API requests and streamline your workflows with secure and reliable PDF API. It's easy to integrate PDF editing, PDF conversion, watermark, OCR, and more features.


# 2 ComPDFKit PDF SDK

The two elements for ComPDFKit:

- **PDF Core API**

The ComPDFKit PDF SDK.Desk can be used independently for document rendering, analysis, text extraction, text search, form filling, annotation creation and manipulation, and much more.
- **PDF View**

The ComPDFKit PDF SDK.Viewer is a utility class that provides the functionality for developers to interact with rendering PDF documents per their requirements. The View Control provides fast and high-quality rendering, zooming, scrolling, and page navigation features.

# 3 Key Features of ComPDFKit 

**Viewer** component offers:

- Standard page display modes, including Single Page, Double Page, Scrolling, and Cover Mode.
- Navigation with thumbnails, outlines, and bookmarks.
- Text search & selection.
- Zoom in and out & Fit-page.
- Switch between different themes, including Dark Mode, Sepia Mode, Reseda Mode, and Custom Color Mode.
- Text reflow.

**Annotations** component offers:

- Create, edit, and remove annotations, including Note, Link, Free Text, Line, Square, Circle, Highlight, Underline, Squiggly, Strikeout, Stamp, Ink, and Sound.
- Support for annotation appearances.
- Import and export annotations to/from XFDF.
- Support for annotation flattening.

**Forms** component offers:

- Create, edit, and remove form fields, including Push Button, Check Box, Radio Button, Text Field, Combo Box, List Box, and Signature.
- Fill PDF Forms.
- Support for PDF form flattening.

**Document Editor** component offers:

- PDF manipulation, including Split pages, Extract pages, and Merge pages.
- Page edit, include: Delete pages, Insert pages, Move pages, Rotate pages, Replace pages, and Exchange pages.
- Document information setting.
- Extract images.

**Content Editor** component offers:

- Programmatically add and remove text in PDFs and make it possible to edit PDFs like Word. Allow selecting text to copy, resize, change colors, text alignment, and the position of text boxes.
- Support editing PDF images like moving, rotating, scaling, mirroring, cropping, replacing, copying, and extracting.
- Undo or redo any change.

**Security** component offers:

- Encrypt and decrypt PDFs, including Permission setting and Password protected.
- Create and remove watermark.
- Redact content including images, text, and vector graphics.
- Create, edit, and remove header & footer, including dates, page numbers, document name, author name, and chapter name.
- Create, edit, and remove bates numbers.
- Create, edit, and remove background that can be a solid color or an image.

**Conversion** component offers:

- PDF to PDF/A.

**Document Comparison **component offers:

- Compare different versions of a document, including overlay comparison and content comparison.


# 4 License

ComPDFKit PDF SDK is a commercial SDK, which requires a license to grant developer permission to release their apps. Each license is only valid for one device ID in development mode. Other flexible licensing options are also supported, please contact [our marketing team](mailto:support@compdf.com) to know more. However, any documents, sample code, or source code distribution from the released package of ComPDFKit PDF SDK to any third party is prohibited.



# 5 Support

## 5.1 Reporting Problems

Thank you for your interest in ComPDFKit PDF SDK, the only easy-to-use but powerful development solution to integrate high quality PDF rendering capabilities to your applications. If you encounter any technical questions or bug issues when using ComPDFKit PDF SDK for Windows, please submit the problem report to the ComPDFKit team. More information would help us to solve your problem:

- ComPDFKit PDF SDK product and version.
- Your operating system and IDE version.
- Detailed descriptions of the problem.
- Any other related information, such as an error screenshot.

## 5.2 Contact Information

**Website:**

- Home Page: [https://www.compdf.com](https://www.compdf.com/)
- API Page: https://api.compdf.com/
- Developer Guides: https://www.compdf.com/guides/pdf-sdk/windows/overview
- API Reference: https://www.compdf.com/guides/pdf-sdk/windows/api-reference/html/3a1f08b6-6ac4-f8b5-bad1-a31c98e96105.htm
- Code Examples: https://www.compdf.com/guides/pdf-sdk/windows/examples

**Contact ComPDFKit:**

- Contact Sales: https://api.compdf.com/contact-us

- Technical Issues Feedback: https://www.compdf.com/support

- Contact Email: [support@compdf.com](mailto:support@compdf.com)


Thanks,
The ComPDFKit Team
